// Code for symbolic objects, to be used in expression templates and symbolic processing
#ifndef ATMOS_SYMBOLIC_OBJECT
#define ATMOS_SYMBOLIC_OBJECT

namespace ATMOS
{
  // The basic symbolic container and operator. Indicates something may be handled in some way by symbolics. 
  class Symbolic
  {
  };
  
  // An individual "symbol", the basic unit of symbolic processing and storage
  class Symbol: public Symbolic
  {
  };

  // A symbolic object of an object type, used in expression templates and for other symbolic purposes
  template <class objectType>
  struct symbolicObject: public Symbolic
  {
    // For defining symbolic operations and containers
    const objectType& get_ref() const
    {
      return static_cast<const objectType&>(*this);
    }
  };
  
  // Base class for a symbolic container of any type (e.g. a container designed to hold symbolic objects, for example, a set of symbolic objects)
  class symbolicContainer
  {
  };
}

#endif
