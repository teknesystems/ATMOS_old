//!Expression templates for binary and unary operations

// Header guard. Skips over entire header if already included
#ifndef ATMOS_STATIC_OPS_INCLUDED
#define ATMOS_STATIC_OPS_INCLUDED

#include "../symbolic_objects.hpp"
#include "../symbolic_operators.hpp"

namespace ATMOS
{
  // A symbolic operation supporting static polymorphism
  template<class T>
  struct staticOperation: public symbolicObject<T>
  {
  };
  
  template<class input1, class input2, class operationType>
  struct binaryOperation: public symbolicObject<binaryOperation<input1,input2,operationType>>
  {
    const input1& left;
    const input2& right;
    const operationType& operation;
  };
  
  template<class input1, class operationType>
  struct unaryOperation: public symbolicObject<unaryOperation<input1,operationType>>
  {
    const input1& operand;
    const operationType& operation;
  };
  
  template<class input1, class operation, template<class...> class symbolic_context>
  struct staticUnaryOperation: public staticOperation<staticUnaryOperation<input1,operation,symbolic_context>>,
			       public symbolic_context<staticUnaryOperation<input1,operation,symbolic_context>>
  {
    
    const input1& operand;
    staticUnaryOperation(const input1& input): operand (input) {}
    template<class T>
    using context = symbolic_context<T>;
  };
  
  template<class input1, class input2, class operation, template<class...> class symbolic_context>
  struct staticBinaryOperation: public staticOperation<staticBinaryOperation<input1,input2,operation,symbolic_context>>,
				public symbolic_context<staticBinaryOperation<input1,input2,operation,symbolic_context>>
  {
    const input1& left;
    const input2& right;
    typedef input1 rightType;
    typedef input2 leftType;
    staticBinaryOperation(const input1& A, const input2&B): left (A), right (B) {}
    template<class T>
    using context = symbolic_context<T>;
  };

  // Wraps nicely onto FMA, used mainly for optimization
  template<class input1, class input2, class input3, class operation, template<class...> class symbolic_context>
  struct staticTernaryOperation: public staticOperation<staticTernaryOperation<input1,input2,input3,operation,symbolic_context>>,
				 public symbolic_context<staticTernaryOperation<input1,input2,input3,operation,symbolic_context>>
  {
    const input1& iacc;
    const input2& left;
    const input3& right;
    typedef input1 accType;
    typedef input2 rightType;
    typedef input3 leftType;
    staticTernaryOperation(const input1& C, const input2& A, const input3& B): iacc(C), left (A), right (B){}
    template<class T>
    using context = symbolic_context<T>;
  };

  // Wraps nicely onto some BLAS functions (including DGEMM with beta = 1), used mainly for optimization
  template<class input1, class input2, class input3, class input4, class operation, template<class...> class symbolic_context>
  struct staticQuaternaryOperation: public staticOperation<staticQuaternaryOperation<input1,input2,input3,input4,operation,symbolic_context>>,
				    public symbolic_context<staticQuaternaryOperation<input1,input2,input3,input4,operation,symbolic_context>>
  {
    const input1& iacc;
    const input2& alpha;
    const input3& left;
    const input4& right;
    typedef input1 accType;
    typedef input2 rightType;
    typedef input3 leftType;
    staticQuaternaryOperation(const input1& C, const input3& S, const input2& A, const input4& B): iacc(C), alpha(S), left (A), right (B){}
    template<class T>
    using context = symbolic_context<T>;
  };

  // Wraps nicely onto DGEMM, used mainly for optimization
  template<class input1, class input2, class input3, class input4, class input5, class operation, template<class...> class symbolic_context>
  struct staticQuinaryOperation: public staticOperation<staticQuinaryOperation<input1,input2,input3,input4,input5,operation,symbolic_context>>,
				 public symbolic_context<staticQuinaryOperation<input1,input2,input3,input4,input5,operation,symbolic_context>>
  {
    const input1& iacc;
    const input2& alpha;
    const input3& left;
    const input4& beta;
    const input5& right;
    typedef input1 accType;
    typedef input2 rightType;
    typedef input3 leftType;
    staticQuinaryOperation(const input1& C, const input2& a, const input3& A, const input4& b, const input5& B): iacc(C), alpha(a), left (A), beta(b), right (B){}
    template<class T>
    using context = symbolic_context<T>;
  };

  struct accumulation_tag
  {
  };
}


#endif
