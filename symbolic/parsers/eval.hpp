//!The core of ATMOS symbolic processing: the "eval" expression, which evaluates a symbolic expression, either: returning a result from the operations, writing a result to an input container/data
//!class R, or OPERATING on the result and R (the result possibly being moved in). This is overloaded in many places to provide more efficient handling of certain expressions. Coupled with
//!intelligent symbolic expression generation (generating more complex symbolic expressions based off contextual data of types, e.g. Group over operations OP, IOP and INV), can generate fast,
//!hand-written equivalent code.
#ifndef ATMOS_EVAL
#define ATMOS_EVAL


#include "../expressions/static_operations.hpp"

namespace ATMOS
{

  // Basic case
  template<class T>
  inline static const T& eval(const T& input) {return input;}

  // Unary operation
  template<class OP, class B, class O = B, template<class...> class C>
  inline static auto eval(const staticUnaryOperation<OP,B,C>& input, O op = O())
  {
    return op.op(eval(input.operand));
  }
  
  // Binary operation
  template<class R, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(const staticBinaryOperation<R,L,B,C>& input, O op = O())
  {
    return op.op(eval(input.left),eval(input.right));
  }
  
  // Ternary operation
  template<class I, class R, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(const staticTernaryOperation<I,R,L,B,C>& input, O op = O())
  {
    return op.op(eval(input.iacc),eval(input.left),eval(input.right));
  }
  
  // Quaternary operation
  template<class I, class A, class R, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(const staticQuaternaryOperation<I,A,R,L,B,C>& input, O op = O())
  {
    return op.op(eval(input.iacc),eval(input.alpha),eval(input.left),eval(input.right));
  }
  
  // Quinary operation
  template<class I, class A, class R, class Be, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(const staticQuinaryOperation<I,A,R,Be,L,B,C>& input, O op = O())
  {
    return op.op(eval(input.iacc),eval(input.alpha),eval(input.left),eval(input.beta),eval(input.right));
  }

    // Basic case
  template<class RT, class T>
  inline static RT& eval(RT& R, const T& input) {R = input; return R;}

  // Unary operation
  template<class RT, class OP, class B, class O = B, template<class...> class C>
  inline static auto eval(RT& R, const staticUnaryOperation<OP,B,C>& input, O op = O())
  {
    return op.op(R,eval(input.operand));
  }
  
  // Binary operation
  template<class RT, class R, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(RT& result, const staticBinaryOperation<R,L,B,C>& input, O op = O())
  {
    return op.op(result,eval(input.left),eval(input.right));
  }
  
  // Ternary operation
  template<class RT, class I, class R, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(RT& result, const staticTernaryOperation<I,R,L,B,C>& input, O op = O())
  {
    return op.op(result,eval(input.iacc),eval(input.left),eval(input.right));
  }
  
  // Quaternary operation
  template<class RT, class I, class A, class R, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(RT& result, const staticQuaternaryOperation<I,A,R,L,B,C>& input, O op = O())
  {
    return op.op(result,eval(input.iacc),eval(input.alpha),eval(input.left),eval(input.right));
  }
  
  // Quinary operation
  template<class RT, class I, class A, class R, class Be, class L, class B, class O = B, template<class...> class C>
  inline static auto eval(RT& result, const staticQuinaryOperation<I,A,R,Be,L,B,C>& input, O op = O())
  {
    return op.op(result,eval(input.iacc),eval(input.alpha),eval(input.left),eval(input.beta),eval(input.right));
  }

  
  
}

#endif
