//!Simple "magmatic parser" for a string of operations over a magma: simply add along a direction (left or right) or directly.
//!Mainly for use in building more complicated parsers
#ifndef ATMOS_MAGMATIC_PARSER
#define ATMOS_MAGMATIC_PARSER

#include "../expressions/static_operations.hpp"

namespace ATMOS
{

  struct magmatic_parser
  {
    template<class RT, class OP, class B>
    inline static void right_magmatic_merge_parse(const RT& M, RT& R, const OP& O, const B& binding)
    {
      O.op_to(R,M);
    }
    template<class T, class T2, class RT, class OP, class B, template<class...> class C>
    inline static void right_magmatic_merge_parse(const staticBinaryOperation<T,T2,B,C>& O, RT& R, const OP& OPR, const B& binding)
    {
      OPR.op_to(R,O.left);
      right_magmatic_merge_parse(O.right,R,OPR,binding);
    }
    template<class T, class T2, class RT, class OP, class B, template<class...> class C>
    inline static void right_magmatic_parse(const staticBinaryOperation<T,T2,B,C>& O, RT& R, const OP& OPR, const B& binding)
    {
      R = O.left;
      right_magmatic_merge_parse(O.right,R,OPR,binding);
    }
    template<class RT, class OP, class B>
    inline static void left_magmatic_merge_parse(const RT& M, RT& R, const OP& O, const B& binding)
    {
      O.op_to(R,M);
    }
    template<class T, class T2, class RT, class OP, class B, template<class...> class C>
    inline static void left_magmatic_merge_parse(const staticBinaryOperation<T,T2,B,C>& O, RT& R, const OP& OPR, const B& binding)
    {
      OPR.op_to(R,O.right);
      right_magmatic_merge_parse(O.left,R,OPR,binding);
    }
    template<class T, class T2, class RT, class OP, class B, template<class...> class C>
    inline static void left_magmatic_parse(const staticBinaryOperation<T,T2,B,C>& O, RT& R, const OP& OPR, const B& binding)
    {
      R = O.right;
      right_magmatic_merge_parse(O.left,R,OPR,binding);
    }
  };

};

#endif
