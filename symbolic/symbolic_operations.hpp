// Automatic generation of symbolic operations for symbolic-operation enabled types (on usage of C++ operators in source):

// Header guard. Skips over entire header if already included
#ifndef ATMOS_SYMBOLIC_OPS
#define ATMOS_SYMBOLIC_OPS

#include "symbolic_objects.hpp"
#include "symbolic_operators.hpp"
#include "magma.hpp"
#include "unary.hpp"
#include <vector>

// Automatic generation of symbolic operations for symbolic-operation enabled types (on usage of C++ operators in source):
// Both symbolic:
// Semi-symbolic addition:
template<class T1, class T2, class OP>
const ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> operator+ (const ATMOS::Magma<T1,OP,ATMOS::ops::add>& right, const ATMOS::Magma<T2,OP,ATMOS::ops::add>& left) {
  return ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> (right.get_ref(), left.get_ref());
}

// Semi-symbolic multiplication:
template<class T1, class T2, class OP>
const ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> operator* (const ATMOS::Magma<T1,OP,ATMOS::ops::multiply>& right, const ATMOS::Magma<T2,OP,ATMOS::ops::multiply>& left) {
  return ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> (right.get_ref(), left.get_ref());
}

// Semi-symbolic division:
template<class T1, class T2, class OP>
const ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> operator- (const ATMOS::Magma<T1,OP,ATMOS::ops::subtract>& right, const ATMOS::Magma<T2,OP,ATMOS::ops::subtract>& left) {
  return ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> (right.get_ref(), left.get_ref());
}

// Semi-symbolic subtraction:
template<class T1, class T2, class OP>
const ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> operator/ (const ATMOS::Magma<T1,OP,ATMOS::ops::divide>& right, const ATMOS::Magma<T2,OP,ATMOS::ops::divide>& left) {
  return ATMOS::staticBinaryOperation<T1,T2,OP,T1::template context> (right.get_ref(), left.get_ref());
}

/*
// Unary operations:
template<class T1, class OP>
const ATMOS::staticUnaryOperation<T1,OP,T1::template context> operator- (const ATMOS::set_unary_operation<T1,OP,ATMOS::ops::negate>& operand)
{
  return ATMOS::staticUnaryOperation<T1,OP,T1::template context>(operand.get_ref());
}

template<class T, class OP>
typename std::enable_if<std::is_base_of<ATMOS::Involution<T,OP,ATMOS::ops::negate>,T>::value,const T&> operator-(const ATMOS::staticUnaryOperation<T,OP,T::template context>& O)
{
  return O.operand;
}
*/

#endif
