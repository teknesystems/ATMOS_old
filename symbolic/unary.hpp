//!The basics of ATMOS symbolic processing for unary operations: defines the combination of a unary operation and a set on which it operates. Examples include symbolic negation, etc.
//!Derived classes specify important additional characteristics
#ifndef ATMOS_UNARY
#define ATMOS_UNARY



namespace ATMOS
{

  template<class T, class operation, class binding>
  struct set_unary_operation
  {
    inline static operation get_op(const binding& B) {return operation();}
    inline static operation get_op(const operation& B) {return operation();}
  };

  template<class T, class operation, class binding = ops::invert>
  struct Involution: public set_unary_operation<T,operation,binding>
  {
    inline static operation get_inv(const binding& B) {return operation();}
    inline static operation get_inv(const operation& B) {return operation();}
  };
  
};

#endif
