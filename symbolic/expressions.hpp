//!ATMOS symbolic expressions, including expression templates and dynamic, symbolic expressions

// Header guard. Skips over entire header if already included
#ifndef ATMOS_SYMBOLIC_EXP
#define ATMOS_SYMBOLIC_EXP

#include "expressions/static_operations.hpp"

#endif
