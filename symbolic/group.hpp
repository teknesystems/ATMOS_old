//!The combination of a magma and its inverse operation (forming a Quasigroup), and additional constraints on the system defining loops and groups
#ifndef ATMOS_GROUP
#define ATMOS_GROUP

#include "symbolic_operators.hpp"
#include "magma.hpp"
#include "unary.hpp"

namespace ATMOS
{

  template<class T, class operation, class inverse_operation, class binding = ops::multiply, class inverse_binding = ops::divide>
  struct has_Quasigroup: public Magma<T,inverse_operation,inverse_binding>
  {
    inline static auto get_op(const inverse_binding& I) {return inverse_operation();}
    inline static auto get_op(const inverse_operation& I) {return inverse_operation();}
    inline static auto get_inv(const binding& B) {return inverse_operation();}
    inline static auto get_inv(const operation& B) {return inverse_operation();}
    inline static auto get_inv(const inverse_operation& I) {return operation();}
    inline static auto get_inv(const inverse_binding& I) {return operation();}
  };

  template<class T, class operation, class inverter, class inverse_operation = ops::solve, class binding = ops::multiply, class inverse_binding = ops::divide, class inverter_binding = ops::invert>
  struct has_Loop: public has_Quasigroup<T,operation,inverse_operation,binding,inverse_binding>, public Involution<T,inverter,inverter_binding>
  {
  };

  template<class T, class operation, class inverter, class inverse_operation = ops::solve,class binding = ops::multiply, class inverse_binding = ops::divide, class inverter_binding = ops::invert>
  struct Group: public has_Loop<T,operation,inverter,inverse_operation,binding,inverse_binding,inverter_binding>, public Monoid<T,operation,binding>
  {
  };

  template<class T, class operation, class inverter, class inverse_operation = ops::solve,class binding = ops::multiply, class inverse_binding = ops::divide, class inverter_binding = ops::invert>
  struct CommutativeGroup: Group<T,operation,inverter,inverse_operation,binding,inverse_binding,inverter_binding>
  {
  };
};

#endif
