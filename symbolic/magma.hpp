//!The basic unit of ATMOS symbolic expression: the magma, or the association of a set with an operation which goes back to that set (e.g. op(X,X) -> X, and hence, furthermore, an op= exists and is
//!equivalent to x = op(x,y) (for x op=y). Other assumptions, even associativity, are not part of a magma, but can be specified through classes which derive from magmas.
//!Compound structures, e.g. structures involving more than one operation and the interactions between them, are generally defined using magmas where the operations map a set to itself.
//!For important examples of these, see group.hpp.
//!Note that all these concepts are statically polymorphic, so a function can take a group over an operation, a group over an external binding, a magma over an operation, etc. Like concepts!
#ifndef ATMOS_MAGMA
#define ATMOS_MAGMA

namespace ATMOS
{

  //!Binds the functor "operation" to the symbolic operator "external_binding" (e.g. binds an elementwise addition functor to the symbolic operator "add"). Also implicitly binds the operation to
  //!"magma_operation" IF an object is a member of only one magma (otherwise the binding or the operation must be referred to explicitly)
  template<class T, class operation, class external_binding = ops::multiply>
  struct Magma
  {
    inline static operation get_op(const external_binding& E) {return operation();}
    inline static operation get_op(const operation& O) {return operation();}
    
    // For defining symbolic operations and containers
    const T& get_ref() const
    {
      return static_cast<const T&>(*this);
    }
  };

  //!Adds the context of a semigroup, which is associative
  template<class T, class O, class B>
  struct Semigroup: public Magma<T,O,B>
  {
  };

  //!Adds the context of a monoid, which is associative and has an identity to look out for
  template<class T, class O, class B>
  struct Monoid: public Semigroup<T,O,B>
  {
  };
  
};

#endif
