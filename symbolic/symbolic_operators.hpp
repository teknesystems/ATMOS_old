// Symbolic operator. The basis of symbolic processing
#ifndef ATMOS_SYMBOLIC_OP
#define ATMOS_SYMBOLIC_OP

#include "symbolic_objects.hpp"
#include "../operation/assignment_functor.hpp"

namespace ATMOS
{
  // A symbolic operator/function. Interfaces an underlying functor or a representation of a standard C++ operation like add
  template<class F>
  struct symbolicOperator: public Symbol
  {
    // For defining symbolic operations and containers
    const F& get_ref() const
    {
      return static_cast<const F&>(*this);
    }
  };

  // Op/op_to wrapper for a functor supporting operator() with op-like functionality
  template<class F, class AF = binary_assignment<F>>
  struct opWrapper: public symbolicOperator<opWrapper<F,AF>>
  {
    template<class OT, class T1, class T2, class OPT = AF>
    inline static OT& op(OT& C, const T1& A, const T2& B, OPT OP = OPT()) {return OP(C,A,B);}
    template<class T1, class T2, class OPT = F>
    inline static auto op(const T1& A, const T2& B, OPT OP = OPT()) -> decltype(OP(A,B)) {return OP(A,B);}
    template<class OT, class IT, class OPT = AF>
    inline static OT& op_to(OT& A, const IT& B, OPT OP = OPT()) {return OP(A,B);}
  };

  namespace ops
  {
    // Indicator of a symbolic functor (a symbol which represents a functor, and is swapped for the appropriate functor at compile-time, throwing an error if said does not exist)
    struct symbolicFunctor
    {
    };
    
    // Standard C++ operations represented as abstract types
    struct standardOperation: public symbolicFunctor
    {
    };

    // Binary operations
    struct add: public standardOperation {};
    struct multiply: public standardOperation {};
    struct subtract: public standardOperation {};
    struct divide: public standardOperation {};
    // Unary operations
    struct invert: public standardOperation {};
    struct negate: public standardOperation {};

    // Indicator that an operator requires solving for (hence, requires searching for a solver) and may not be implemented
    struct solve: public symbolicFunctor {};
  }
  struct symbolicOperation: public Symbolic
  {
  };
}

#endif
