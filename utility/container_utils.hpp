//!Utilities for moving data between containers and to/from locations in memory (e.g. pointers) for use with ATMOS classes
//!Generally falls back to std::copy for the appropriate iterators for a container, or 
#ifndef ATMOS_CONT_UTILS
#define ATMOS_CONT_UTILS

#include <iterator>
#include <algorithm>
#include <valarray>
#include <array>
#include <list>

namespace ATMOS
{
  template<class CT>
  inline auto container_beginning(CT& C) -> decltype(std::begin(C))
  {
    return std::begin(C);
  }
  template<class T>
  inline T* container_beginning(T* P) {return P;}

  template<class CT>
  inline auto container_end(CT& C) -> decltype(std::end(C)) {return std::end(C);}

  template<class CT>
  inline void resize_container(CT& C,std::size_t N) {C.resize(N);}

  template<class T>
  inline void resize_container(T* P, std::size_t N) {}

  template<class CT>
  inline void reserve_container(CT& C, std::size_t N) {C.reserve(N);}

  template<class T>
  inline void reserve_container(std::valarray<T>& C, std::size_t N) {C.resize(N);}

  template<class T>
  inline void reserve_container(std::list<T>& C, std::size_t N) {} //No-op

  template<class T, std::size_t S>
  inline void reserve_container(std::array<T,S>& A, std::size_t N) {} //No-op

  template<class T>
  inline void reserve_container(T* P, std::size_t N) {}
  
  template<class OT, class IT>
  inline void copy_to_container(OT& OUT, const IT& IN)
  {
    std::copy(container_beginning(IN),container_end(IN),container_beginning(OUT));
  }

  template<class CT>
  inline auto rewrite_front_iterator(CT& C) -> decltype(std::back_inserter(C)) {return std::back_inserter(C);}

  template<class T>
  inline T* rewrite_front_iterator(T* P) {return P;}

  template<class T>
  inline auto rewrite_front_iterator(std::valarray<T>& C) -> decltype(std::begin(C)) {return std::begin(C);}

  template<class T, std::size_t N>
  inline auto rewrite_front_iterator(std::array<T,N>& C) -> decltype(std::begin(C)) {return std::begin(C);}

  template<class CT>
  inline auto container_size(CT& C) -> decltype(C.size()) {return C.size();}
  
  template<class OT, class IT>
  inline void empty_copy_to_container(OT& OUT, const IT& IN)
  {
    reserve_container(OUT,container_size(IN));
    std::copy(container_beginning(IN),container_end(IN),rewrite_front_iterator(OUT));
  }

  template<class CT>
  inline auto clear_container(CT& C) -> decltype(C.clear()) {C.clear();}

  template<class CT>
  inline void clear_container(const CT& C) {}
  
  template<class CT>
  inline auto getElement(CT& C, std::size_t N) -> decltype(C[N])
  {
    return C[N];
  }

  template<class CT>
  inline auto getConstElement(const CT& C, std::size_t N) -> decltype(C[N])
  {
    return C[N];
  }

  template<class T>
  inline auto getElement(std::list<T>& L, std::size_t N) -> decltype(*(L.begin()))
  {
    return *std::next(L.begin(),N);
  }

  template<class T>
  inline auto getElement(const std::list<T>& L, std::size_t N) -> decltype(*(L.begin()))
  {
    return *std::next(L.begin(),N);
  }

  template<class T>
  inline auto getConstElement(const std::list<T>& L, std::size_t N) -> decltype(*(L.begin()))
  {
    return *std::next(L.begin(),N);
  }

  template<class T>
  inline auto container_size(const T& C) -> decltype(C.size()) {return C.size();}
}

#endif
