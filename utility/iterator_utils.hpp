//!Utilities to make better use of iterators
#ifndef ATMOS_ITER_UTILS
#define ATMOS_ITER_UTILS

#include <iterator>

namespace ATMOS
{
  namespace iter
  {
    template<class IT>
    inline void advance(IT& A, std::size_t N = 1) {std::advance(A,N);}
    template<class CT>
    inline void advance(const std::back_insert_iterator<CT>& B, const std::size_t& N) {} // No-op

  }
}

#endif
