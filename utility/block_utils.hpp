//!Utilities for use with short "blocks" or "chunks" of an array
#ifndef ATMOS_BLOCK_UTIL
#define ATMOS_BLOCK_UTIL

#include "iterator_utils.hpp"
#include <algorithm>

namespace ATMOS
{
  template<class checker>
  struct check_first
  {
    checker check;
    template<class IT1, class IT2>
    inline bool operator() (IT1 A, IT2 B, const std::size_t& stride)
    {
      return check(*A,*B);
    }
    template<class T1, class T2>
    inline bool operator() (const T1& A, const T2& B)
    {
      return check(A,B);
    }
  };

  struct write_chunk
  {
    template<class IT1, class OIT>
    inline void operator() (IT1 A, IT1 AE, OIT O,std::size_t stride)
    {
      std::copy(A,AE,O);
    }
  };

  struct noop
  {
    inline void operator()(...) {}
  };

  template<class operation>
  struct write_result_last
  {
    operation op;
    template<class IT1, class IT2, class OIT>
    inline void operator() (IT1 A, IT1 ACE, IT2 B, IT2 BCE, OIT O, std::size_t stride)
    {
      auto E = std::next(A,stride-1);
      std::copy(A,E,O);
      iter::advance(O,stride-1);
      *O = op(*E,*(std::next(B,stride-1)));
    }
  };
};

#endif
