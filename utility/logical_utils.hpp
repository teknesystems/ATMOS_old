//!Useful logical functors for plugging into ATMOS algorithms
#ifndef ATMOS_LOGICAL_UTILS
#define ATMOS_LOGICAL_UTILS

namespace ATMOS
{
  struct always_false
  {
    inline bool operator() (...) {return false;}
  };
  struct always_true
  {
    inline bool operator() (...) {return true;}
  };
}

#endif
