//!Basic mergesort implementation for strided arrays, supporting "dragging along" chunks of elements as well. Also supports sorting ONE array, but keeping track of the changes,
//!applying the same to another array efficiently (and/or directly)
//!Using regular std::sort with std::pair/another type and an appropriate rule is another possible choice
#ifndef ATMOS_ARRAY_MERGE
#define ATMOS_ARRAY_MERGE

#include <algorithm>
#include <iterator>
#include <cstddef>
#include "../iterator_utils.hpp"
#include "../block_utils.hpp"
#include "../logical_utils.hpp"

namespace ATMOS
{
  namespace array
  {
    template<class checker>
    struct check_first
    {
      checker check;
      template<class IT1, class IT2>
      inline bool operator() (IT1 A, IT2 B, const std::size_t& stride)
      {
	return check(*A,*B);
      }
    };
    template< class cmp = check_first<std::less<>>, class on_y = write_chunk, class on_n = on_y, class equalitycheck = always_false, class on_equality = noop, class on_remainder = on_y,
	      class OIT, class IT1, class IT2>
    inline void merge(IT1 A, IT1 AE, IT2 B, IT2 BE, OIT O, std::size_t stride,
		      cmp order = cmp(), equalitycheck eqchk = equalitycheck(), 
		      on_equality on_eq = on_equality(), on_y on_true = on_y(), on_n on_false = on_n(), on_remainder on_r = on_remainder())
    {
      auto ACE = std::next(A,stride);
      auto BCE = std::next(B,stride);
      while((ACE <= AE)&&(BCE <= BE))
	{
	  if(order(A,B,stride))
	    {
	      on_true(A,ACE,O,stride);
	      iter::advance(O,stride);
	      iter::advance(A,stride);
	      iter::advance(ACE,stride);
	    }
	  else if(eqchk(A,B,stride))
	    {
	      on_eq(A,ACE,B,BCE,O,stride);
	      iter::advance(O,stride);
	      iter::advance(A,stride);
	      iter::advance(ACE,stride);
	      iter::advance(B,stride);
	      iter::advance(BCE,stride);
	  }
	  else
	    {
	      on_false(B,BCE,O,stride);
	      iter::advance(O,stride);
	      iter::advance(B,stride);
	      iter::advance(BCE,stride);
	    }
	}
      if(A < AE)
	{
	  on_r(A,AE,O,stride);
	}
      else if(B < BE)
	{
	  on_r(B,BE,O,stride);
      }
    }
    template<class ORD = check_first<std::less<>>, class EQ = always_false, class OEQ = noop, class IT1, class IT2>
    inline void mergesort_chunks(IT1 A, IT2 WA, std::size_t strides, std::size_t stride = 1, bool to_work = false, bool written = true, ORD C = ORD(), EQ E = EQ(), OEQ T = OEQ())
    {
      if(strides <= 1)
	{ 
	  if(to_work&&(!written))
	    {
	      auto E = std::next(A,stride);
	      std::copy(A,E,WA);
	    }
	}
      else
	{
	  auto B = std::next(A,(strides/2)*stride);
	  auto WB = std::next(WA,(strides/2)*stride);
	  mergesort_chunks(A,WA,strides/2,stride,!to_work,written,C,E,T);
	  mergesort_chunks(B,WB,(strides+1)/2,stride,!to_work,written,C,E,T);
	  if(to_work)
	    merge(A,B,B,std::next(A,strides*stride),WA,stride,C,E,T);
	  else
	    merge(WA,WB,WB,std::next(WA,strides*stride),A,stride,C,E,T);
	}
    }
    template<class ORD = check_first<std::less<>>, class EQ = always_false, class OEQ = noop, class IT1, class IT2>
    inline void mergesort(IT1 A, IT2 WA, std::size_t N, std::size_t stride = 1, bool to_work = false, bool written = true, ORD C = ORD(), EQ E = EQ(), OEQ T = OEQ())
    {
      mergesort_chunks(A,WA,N/stride,stride,to_work,written,C,E,T);
    }
  }

  namespace algorithms
  {
    struct mergesort
    {
      template<class IT1, class IT2, class ORD = check_first<std::less<>>, class EQ = always_false, class OEQ = noop>
      inline void operator() (IT1 A, IT1 AE, IT2 W, IT2 WAE, std::size_t stride = 1, bool to_work = false, bool written = true, ORD ORDER = ORD(), EQ EQL = EQ(), OEQ ACT = OEQ())
      {
	auto N = std::abs(std::min(std::distance(A,AE),std::distance(W,WAE)));
	array::mergesort(A,W,N,stride,to_work,written,ORDER,EQL,ACT);
      }
      template<class CT1, class CT2, class ORD = check_first<std::less<>>, class EQ = always_false, class OEQ = noop>
      inline void operator() (CT1 A, CT2 W, std::size_t stride = 1, bool to_work = false, bool written = true, ORD ORDER = ORD(), EQ EQL = EQ(), OEQ ACT = OEQ())
      {
	array::mergesort(container_beginning(A),container_beginning(W),std::min(container_size(A),container_size(W)),stride,to_work,written,ORDER,EQL,ACT);
      }
      template<class IT1, class IT2, class ORD = check_first<std::less<>>, class EQ = always_false, class OEQ = noop>
      inline void operator() (IT1 A, IT2 W, std::size_t N, std::size_t stride = 1, bool to_work = false, bool written = true, ORD ORDER = ORD(), EQ EQL = EQ(), OEQ ACT = OEQ())
      {
	array::mergesort(A,W,N,stride,to_work,written,ORDER,EQL,ACT);
      }
    };
  }
}

#endif
