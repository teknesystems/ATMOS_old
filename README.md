Automatically Tuned Mathematical Object System (ATMOS): A data handling, expression template, symbolic computation and general mathematical object handling library. 
An overhaul of the old tmatlib classes and type system.

