//!A simple interface for 2D element access over a dynamic 1D array, and a generalized 2D array interface with an arbitrary (template) "source"
#ifndef ATMOS_SIMPLEMATRIX
#define ATMOS_SIMPLEMATRIX

#include <cstddef>
#include "arrayvector.hpp"

namespace ATMOS
{

  struct matrix_context
  {
    std::size_t split; // Split each row/column at which element
    bool direction; //Row-major or column major
    matrix_context(): direction(true) {}
    matrix_context(std::size_t s, bool d = true): split (s), direction (d) {}
  };
  
  template<class elementType, class containerType>
  struct simplematrix: public matrix_context, public arrayvector<elementType, containerType>
  {
    // Standard constructors:
    // Trivial
    simplematrix() {}
    // Specification
    simplematrix(std::size_t r, std::size_t c, bool d = true): matrix_context(d?r:c,d), arrayvector<elementType,containerType>(r*c) {}
    simplematrix(std::size_t r, std::size_t c, const elementType& V, bool d = true): matrix_context(d?r:c,d), arrayvector<elementType,containerType>(r*c,V) {}
    // Iterator
    template<class IT>
    simplematrix(IT src, std::size_t r, std::size_t c, bool d = true): matrix_context(d?r:c,d), arrayvector<elementType,containerType>(src,r*c) {}
    template<class IT>
    simplematrix(IT A, IT B, std::size_t s, bool d = true): matrix_context(s,d), arrayvector<elementType,containerType>(A,B) {}
    // Container (including initializer list)
    template<class CT>
    simplematrix(const CT& C, std::size_t s, bool d = true): matrix_context(s,d), arrayvector<elementType,containerType>(C) {}
    
    // Using declaration for members
    using arrayvector<elementType, containerType>::elements;
    using arrayvector<elementType, containerType>::size;
    using matrix_context::direction;
    using matrix_context::split;

    // Matrix information functions
    std::size_t rows() const {return direction?(size/split):split;}
    std::size_t cols() const {return direction?split:(size/split);}

    // 2-dimensional access (1D access via arrayvector interface)
    elementType& getElement(std::size_t r, std::size_t c) {return getElement(split*(direction?r:c)+(direction?c:r));}
    const elementType& getConstElement(std::size_t r, std::size_t c) const {return getConstElement(split*(direction?r:c)+(direction?c:r));}
  };

  // Standardized matrix interface:
  template<class elementType, class containerType, class matrixType = simplematrix<elementType,containerType>>
  struct smatrix
  {
    matrixType elements; // Underlying matrix container
    
    // Forwarded standard constructors:
    // Trivial
    smatrix(): elements() {}
    // Specification
    smatrix(std::size_t r, std::size_t c, bool d = true): elements(r,c,d) {}
    smatrix(std::size_t r, std::size_t c, const elementType& V, bool d = true): elements(r,c,V,d) {}
    // Iterator
    template<class IT>
    smatrix(IT src, std::size_t r, std::size_t c, bool d = true): elements(src,r,c,d) {}
    template<class IT>
    smatrix(IT A, IT B, std::size_t s, bool d = true): elements(A,B,s,d) {}
    // Container (including initializer list)
    template<class CT>
    smatrix(const CT& C, std::size_t s, bool d = true): elements(C,s,d) {}

    // Forwarded standard functions:
    // Matrix information functions
    std::size_t rows() const {return elements.rows();}
    std::size_t cols() const {return elements.cols();}

    // 2-dimensional access (1D access via arrayvector interface)
    elementType& getElement(std::size_t r, std::size_t c) {return elements.getElement(r,c);}
    const elementType& getConstElement(std::size_t r, std::size_t c) const {return elements.getConstElement(r,c);}
  };
};

#endif
