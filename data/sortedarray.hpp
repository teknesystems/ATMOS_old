//!ATMOS wrapper for sorted 1-dimensional arrays, with strided sort as an option (e.g. sorting blocks of 2 elements by every second element, or by added value).
#ifndef ATMOS_SORTEDARRAY
#define ATMOS_SORTEDARRAY

#include <cstddef>
#include "../utility/logical_utils.hpp"
#include "../utility/array_utils/mergesort.hpp"

namespace ATMOS
{
  template<class elementType, class containerType = std::vector<elementType>, int blocksize = 1, // Data
	   class sort_order = check_first<std::less<>>, class equality = always_false, class on_equality = noop, // Sorting
	   class sorter_imp = algorithms::mergesort, bool cont_resizeable = true> // Implementation
  struct sortedarray: public arrayvector<elementType,containerType,cont_resizeable>
  {
    
    using arrayvector<elementType,containerType,cont_resizeable>::elements; // Underlying data
    // Data processors
    sorter_imp sorter; // Processes raw input data
    sort_order order; // The order in which items are to be sorted
    
    // Trivial constructor
    sortedarray() {}

    // Sorting function
    inline void sort()
    {
      std::vector<elementType> W = elements;
      sorter(container_beginning(elements),container_end(elements),container_beginning(W),container_end(W),blocksize,false,true,order,equality(),on_equality());
    }

    // Sorted constructors (from container)
    template<class IT>
    sortedarray(IT A, IT B)
    {
      reserve_container(elements,std::distance(A,B));
      auto D = rewrite_front_iterator(elements);
      std::copy(A,B,D);
      sort();
    }
    template<class IT>
    sortedarray(IT A, std::size_t N): sortedarray(A,std::next(A,N)) {}
    template<class CT>
    sortedarray(const CT& C): sortedarray(container_beginning(C),container_end(C)) {}
    
  };
};

#endif
