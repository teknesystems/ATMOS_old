//!The basic ATMOS wrapper for a 1D underlying array. Can then be statically or dynamically generalized to multiple dimensions. Can be dynamically resizeable, or not.
//!Designed to provide a unified, ATMOS-friendly interface for holding vectors, matrices and arbitrary array-based datatypes (tensors, bigints, etc.), and to allow inheriting from this interface
//!safely (unlike with STL types)
#ifndef ATMOS_ARRAYVECTOR
#define ATMOS_ARRAYVECTOR

// Standard Types
#include <vector>
#include <valarray>
#include <initializer_list>
#include <cstddef>
// STL functions
#include <algorithm>
#include <iterator>
// Data utilities
#include "../utility/container_utils.hpp"
// Metaprogramming utilities
#include "../meta/switches.hpp"
#include "../meta/syntax.hpp"

namespace ATMOS
{ 
  template<class elementType, class containerType = std::vector<elementType>, bool cont_resizeable = true> //TODO: add template based "resizeability detector"
  struct arrayvector
  {
    containerType elements; // Underlying data. Can be publically manipulated
    // Constructors:
    
    // Trivial
    arrayvector() {}

    // Specification
    // Length (default/0 value)
    arrayvector(std::size_t N): elements(N) {}
    // Length, val
    arrayvector(std::size_t N, const elementType& val): elements(N,val) {}
    // Initializer lists
    arrayvector(std::initializer_list<elementType> I): elements(I){}
    
    // Iterator/pointer
    template<class IT>
    arrayvector(IT A, IT B): elements(A,B) {}
    template<class IT>
    arrayvector(IT input, std::size_t N): elements(input,std::next(input,N)) {}

    // Container
    arrayvector(const containerType& C): elements(C) {}
    
    // Utility functions:
    // Get iterators
    auto begin() -> decltype(container_beginning(elements)) {return container_beginning(elements);}
    auto end() -> decltype(container_end(elements)) {return container_end(elements);}
    // Size, resize and reserve (as applicable to underlying container)
    std::size_t size() const {return elements.size();}
    void resize(std::size_t n) {resize_container(elements,n);}
    void reserve(std::size_t n) {reserve_container(elements,n);}
    
    // Change container contents
    void swapcontainer(containerType& input)
    {
      using std::swap;
      swap(elements,input);
    }
    void assigncontainer(const containerType& input)
    {
      elements = input;
    }
    template<class CT>
    void assigncontainer(const CT& input)
    {
      resize_container(container_size(input));
      copy_to_container(elements,input);
    }
    template<class IT>
    void assigncontainer(IT A, IT B)
    {
      if(B > A)
	resize_container(elements,B-A);
      std::copy(A,B,container_beginning(elements));
    }
    template<class IT>
    void assigncontainer(IT P, std::size_t N)
    {
      resize_container(elements,N);
      std::copy(P,std::next(P,N),container_beginning(elements));
    }

    // Access and interface:
    auto operator[] (std::size_t N) -> decltype(getElement(elements,N)) {return getElement(elements,N);}
    elementType& getElement(std::size_t N) {return getElement(elements,N);}
    const elementType& getConstElement(std::size_t N) const {return getConstElement(elements,N);}
  };





  
  // Valarray specialization (mostly duplicated)
  template<class elementType>
  struct arrayvector<elementType,std::valarray<elementType>>
  {
    std::valarray<elementType> elements; // Underlying data. Can be publically manipulated
    // Constructors:
    
    // Trivial
    arrayvector() {}

    // Specification
    // Length (default/0 value)
    arrayvector(std::size_t N): elements(N) {}
    // Length, val
    arrayvector(std::size_t N, const elementType& val): elements(val,N) {}
    // Initializer lists
    arrayvector(std::initializer_list<elementType> I): elements(I){}

    // Pointer
    arrayvector(elementType* P, std::size_t N): elements(P,N){}
    // General iterator
    template<class IT, class CAT = typename IT::iterator_category>
    arrayvector(IT A, IT B, CAT C = CAT())
    {
      elements.resize(std::distance(A,B));
      std::copy(A,B,std::begin(elements));
    }
    template<class IT, class CAT = typename IT::iterator_category>
    arrayvector(IT input, std::size_t N, CAT C = CAT())
    {
      elements.resize(N);
      std::copy(input,std::next(input,N),std::begin(elements));
    }

    // Container
    arrayvector(const std::valarray<elementType>& C): elements(C) {}
    template<class CT>
    arrayvector(const CT& C)
    {
      empty_copy_to_container(elements,C);
    }
    
    // Utility functions:
    // Get iterators
    auto begin() -> decltype(container_beginning(elements)) {return container_beginning(elements);}
    auto end() -> decltype(container_end(elements)) {return container_end(elements);}
    auto size() -> decltype(elements.size()) const {return elements.size();}
    
    // Change container contents
    void swapcontainer(std::valarray<elementType>& input)
    {
      using std::swap;
      swap(elements,input);
    }
    void assigncontainer(const std::valarray<elementType>& input)
    {
      elements = input;
    }
    template<class CT>
    void assigncontainer(const CT& input)
    {
      resize_container(container_size(input));
      copy_to_container(elements,input);
    }
    template<class IT>
    void assigncontainer(IT A, IT B)
    {
      if(B > A)
	resize_container(elements,B-A);
      std::copy(A,B,container_beginning(elements));
    }
    template<class IT>
    void assigncontainer(IT P, std::size_t N)
    {
      resize_container(elements,N);
      std::copy(P,std::next(P,N),container_beginning(elements));
    }

    // Access and interface:
    auto operator[] (std::size_t N) -> decltype(getElement(elements,N)) {return getElement(elements,N);}
    elementType& getElement(std::size_t N) {return getElement(elements,N);}
    const elementType& getConstElement(std::size_t N) const {return getConstElement(elements,N);}
  };

  
  template<class ET, class CT>
  inline auto container_beginning(const arrayvector<ET,CT>& C) -> decltype(C.elements.begin())
  {
    return C.elements.begin();
  }
}

#endif
