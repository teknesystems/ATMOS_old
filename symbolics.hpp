//!The core of the symbolic functionality of ATMOS. Includes basic symbolic constructs and expression templates, as well as symbolic constants.
//!Also includes vectorized operations, and hence the core of ATMOS data handling

// Header guard. Skips over entire header if already included
#ifndef ATMOS_SYMBOLICS
#define ATMOS_SYMBOLICS

#include "symbolic/symbolic_objects.hpp"
#include "symbolic/symbolic_operators.hpp"
#include "symbolic/expressions.hpp"
#include "symbolic/symbolic_operations.hpp"
#include "symbolic/parsers/eval.hpp"

#endif
