//!Utility for "printing" ATMOS datastructures (vectors, matrices, etc.) to various forms of I/O quickly and efficiencly (in human readable form)
#ifndef ATMOS_PRINTER
#define ATMOS_PRINTER

// Standard library dependencies
#include <string>
#include <sstream>
#include <type_traits>
#include <iterator>

namespace ATMOS
{
  namespace printer
  {
    struct Style
    {
    };
  
    struct arrayStyle: public Style
    {
      inline static std::string begin() {return "{";}
      inline static std::string afterNum() {return "";}
      inline static std::string beforeNum() {return "";}
      inline static std::string end() {return "}";}
      inline static std::string sepr() {return ",";}
    };
  
    struct vectorStyle: public arrayStyle
    {
      inline static std::string begin() {return "<";}
      inline static std::string end() {return ">";}
    };

    struct matrixStyle: public arrayStyle
    {
      arrayStyle substyle;
      inline static std::string transpose_indicator() {return "^T";}
    };
  
    struct arrayPrinter
    {
      template<class styleType = arrayStyle, class streamType, class IT>
      inline static streamType& print_to(streamType& printer, IT A, IT B, styleType style = styleType())
      {
	printer<<style.begin();
	if(A < B)
	  {
	    printer<<style.beforeNum();
	    printer<<*A;
	    printer<<style.afterNum();
	    A++;
	  }
	while(A < B)
	  {
	    printer<<style.sepr();
	    printer<<style.beforeNum();
	    printer<<*A;
	    printer<<style.afterNum();
	    A++;
	  }
	printer<<style.end();
	return printer;
      }
      template<class streamType, class CT, class styleType = arrayStyle>
      inline static streamType& print_to(streamType& printer, CT C, styleType style = styleType())
      {
	return print_to(printer,container_beginning(C),container_end(C),style);
      }
      template<class streamType, class IT, class styleType = arrayStyle>
      inline static streamType& print_to(streamType& printer, IT A, std::size_t N, styleType style = styleType())
      {
	return print_to(printer,A,std::next(A,N),style);
      }
    };

    struct matrixPrinter
    {
      template<class STYLE, class ST, class IT>
      inline static ST& print_to(ST& printer, IT A, IT B, std::size_t s, STYLE style = STYLE())
      {
	printer<<style.begin();
	if(s!=0)
	  {
	    auto NA = std::next(A,s);
	    if(NA <= B)
	      {
		printer<<style.beforeNum();
		arrayPrinter(printer,A,NA,style.substyle());
		printer<<style.afterNum();
		std::advance(A,s);
		std::advance(NA,s);
	      }
	    while(NA <= B)
	      {
		printer<<style.sepr();
		printer<<style.beforeNum();
		arrayPrinter(printer,A,NA,style.substyle());
		printer<<style.afterNum();
		std::advance(A,s);
		std::advance(NA,s);
	      }
	  }
	printer<<style.end();
	return printer;
      }
    };
  }
}

#endif
