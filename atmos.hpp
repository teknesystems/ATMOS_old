//!Header file which includes the core functionality of ATMOS. If you just need an individual feature, include that individual header. If you need ALL of ATMOS included, include this file with the
//!macro ATMOS_INCLUDE_ALL defined.

#ifndef ATMOS_CORE_INCLUDED
#define ATMOS_CORE_INCLUDED

#define ATMOS_VERSION "0.0.1"

// Symbolics
#include "symbolics.hpp"
#include "symbolic/magma.hpp"
#include "symbolic/unary.hpp"
#include "symbolic/group.hpp"
// Data
#include "data.hpp"
#include "data/sortedarray.hpp"
#include "data/simplematrix.hpp"
// Operations and objects
#include "operation/binary_container.hpp"
#include "operation/unary_container.hpp"
#include "objects/magma_member.hpp"
#include "objects/contextual_object.hpp"
// Utility
#include "utility/array_utils/mergesort.hpp"
// IO
#include "IO/printers.hpp"

#ifdef ATMOS_INCLUDE_ALL



#endif

#endif
