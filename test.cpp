// Test program, both to check if ATMOS compiles and to provide rudimentary runtime tests (more advanced tests should be performed using the tmatlib test.cpp instead)

#include <iostream>
#include <complex>
#define ATMOS_INCLUDE_ALL
#include "atmos.hpp"


template<class ST>
void print_details(ST& S)
{
  S<<"Version "<<ATMOS_VERSION<<" compiled successfully from: "<<__FILE__<<" on "<<__TIME__<<", "<<__DATE__
	   <<" using C++ version "<<__cplusplus<<"\n";
  #ifdef CBLAS_H
  S<<"BLAS interface enabled.\n";
  #endif
}


template<class T>
using test_context = ATMOS::Magma<T,ATMOS::simple_binary_container_operation<>,ATMOS::ops::add>;

template<class T>
using test_group = ATMOS::CommutativeGroup<T,ATMOS::simple_binary_container_operation<>,ATMOS::simple_unary_container_operation<>,ATMOS::simple_binary_container_operation<ATMOS::iterator_elementwise_binary_kernel<std::minus<>>>,ATMOS::ops::add,ATMOS::ops::subtract,ATMOS::ops::negate>;

void print_welcome()
{
  std::cout<<"Welcome to the ATMOS testing utility!\n";
  print_details(std::cout);
}

int main()
{
  print_welcome();
  using ATMOS::printer::arrayPrinter;
  ATMOS::arrayvector<int,std::valarray<int>> S (5,1);
  ATMOS::arrayvector<int> Z (7,1);
  arrayPrinter::print_to(std::cout,S)<<"\n";
  arrayPrinter::print_to(std::cout,Z)<<"\n";
  auto F = ATMOS::simple_binary_container_operation<>::op(Z,Z);
  arrayPrinter::print_to(std::cout,F)<<"\n";


  ATMOS::contextualObject<decltype(F),test_context> M = F.elements;
  auto O = M + M + M;
  decltype(M) M2 = O;
  arrayPrinter::print_to(std::cout,M2)<<"\n";

  ATMOS::contextualObject<decltype(F),test_group> G = F.elements;
  arrayPrinter::print_to(std::cout,G)<<"\n";
  decltype(G) G2 = G + G - G;
  arrayPrinter::print_to(std::cout,G2)<<"\n";
}
