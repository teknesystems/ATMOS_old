//!An object composed of an inheritable, ATMOS compatible data-container and a mathematical context used in generating and parsing symbolic expressions at compile-time
//!Can be used to form vectors (for example, with an underlying arrayvector container), matrices (with an underlying simplematrix or other container), elements of groups and magmas, and more.
#ifndef ATMOS_CONTEXT_OBJ
#define ATMOS_CONTEXT_OBJ

namespace ATMOS
{

  template<class UT, template<class...> class context_used>
  struct contextualObject: public UT, public context_used<contextualObject<UT,context_used>>, public symbolicObject<contextualObject<UT,context_used>>
  {
    using UT::UT; // Inheriting data-based constructors from underlying data container (e.g. making a matrix from an iterator instead of from a symbolic operation, which this class would handle)
    using UT::elements; // Giving visibility to the data of the underlying type used

    contextualObject() {} // Allowing empty constructor for the underlying data container

    // Making context visible for external functions (for example, to determine the context of a symbolic operation)
    template<class T>
    using context = context_used<T>;

    // Generating a contextual object from a symbolic operation
    template<class T>
    contextualObject(const staticOperation<T>& O)
    {
      eval(*this,O.get_ref());
    }
  };
  
}

#endif
