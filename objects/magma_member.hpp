//!A object which is a member of a set on which a magma is defined
//!A basic ATMOS symbolic object
#ifndef ATMOS_MAGMA_MEMBER
#define ATMOS_MAGMA_MEMBER

#include "../symbolic/symbolic_operators.hpp"
#include "../symbolic/expressions/static_operations.hpp"
#include "../symbolic/magma.hpp"
#include "../symbolic/parsers/magmatic.hpp"

namespace ATMOS
{
  template<class UT, // Underlying ATMOS inheritable data container
	   class OP, // Operation of the magma
	   class binding = ops::multiply>
  struct magma_member: public UT, public Magma<magma_member<UT,OP,binding>,OP,binding>, public symbolicObject<magma_member<UT,OP,binding>>
  {
    using UT::UT;
    using UT::elements;
    magma_member() {} // Allowing empty constructor

    template<class T, template<class...> class C>
    magma_member(const staticBinaryOperation<T,magma_member,binding,C>& O)
    {
      magmatic_parser::left_magmatic_parse(O,*this,OP(),binding());
    };
  };
}

#endif
