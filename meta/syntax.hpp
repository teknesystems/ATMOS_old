//!Template classes for adapting programs to various interfaces

#ifndef ATMOS_META_SYNTAX
#define ATMOS_META_SYNTAX

#include <valarray>

namespace ATMOS
{
  namespace meta
  {
    template<class T>
    struct valarray_syntax
    {
      static constexpr bool value = false;
    };
    
    template<class T>
    struct valarray_syntax<std::valarray<T>>
    {
      static constexpr bool value = true;
    };
  }
}

#endif
