//!Template based "switches" for metaprogramming and syntactic sugar
//!Used, for example, to partially specialize constructors for std::valarray whilst minimizing duplication (due to its interface being different from other STL containers like std::vector)
#ifndef ATMOS_META_SWITCHES_INCLUDED
#define ATMOS_META_SWITCHES_INCLUDED

namespace ATMOS
{
  namespace meta
  {
    template<bool side>
    struct binary_switch
    {
      template<class T1, class T2>
      inline static T1 value(T1 A, T2 B) {return A;}
    };
    template<>
    struct binary_switch<false>
    {
      template<class T1, class T2>
      inline static T2 value(T1 A, T2 B) {return B;}
    };
  }
}

#endif
