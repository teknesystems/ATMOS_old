//!ATMOS data handler basics, including contiguous data in N dimensions, contiguous data in variable dimensions, contiguous linked data (e.g. std::list) and sparse storage.
//!Should allow interpreting STL and STL-like containers, raw pointers, references to containers and pointers to containers as complex data structures.

#ifndef ATMOS_DATA_INCLUDED
#define ATMOS_DATA_INCLUDED

#include "data/arrayvector.hpp"

namespace ATMOS
{

  enum dataDirection {rowMajor = 101, colMajor = 102, notTranspose = 111, transpose = 112, conjtranspose = 113, upper = 121, lower = 122, left = 141, right = 142};
  
}

#endif
