//!Simple binary operations on containers, optionally (by default) allowing only containers of the same length. Passes through a container to an iterator kernel after length checking.
#ifndef ATMOS_BINARY_CONTAINER
#define ATMOS_BINARY_CONTAINER

#include <algorithm>
#include <functional>

#include "kernels/vector/elementwise_binary.hpp"
#include "../utility/container_utils.hpp"

namespace ATMOS
{
  
  // Assumes associativity and totality (valid inputs lead to valid output, in this context)
  // Here, only one contextual piece of data is passed: the array length
  // More complicated operations (e.g. more contextual data, say, matrix multiplication with m, n and p parameters)
  // require either more complicated basis handlers or using another operation interface/handler (taking in more complex types with more context)
  template<class kernel = iterator_elementwise_binary_kernel<std::plus<>>, // The kernel to perform the operation (here simply add each element of each array)
	   bool strict = true> // Require the same length for both input arrays
  struct simple_binary_container_operation: public kernel
  {
    // Importing operations from the kernel for use on iterators
    using kernel::op;
    using kernel::op_to;
    using kernel::op_range_to;
  
    // Handles container input from vector classes and passes iterator input through to kernel
    template<class CT1, class CT2>
    static inline bool op_to(CT1& A, const CT2& B)
    {
      auto PA = container_beginning(A);
      auto PB = container_beginning(B);
      if(strict)
	{
	  auto n = container_size(A);
	  if(n == container_size(B))
	    {
	      op_to(PA,PB,n);
	    }
	  else
	    {
	      return false; // Indicate operation failure
	    }
	}
      else
	{
	  op_to(PA,PB,std::min(container_size(A),container_size(B)));
	}
      return true;
    }
    // Writing operation result to third container
    template<class CT1, class CT2, class containerThree>
    static inline bool op(containerThree& C, const CT1& A, const CT2& B)
    {
      // Getting pointers to A and B
      auto PA = container_beginning(A);
      auto PB = container_beginning(B);
      auto n = container_size(A);
      if(strict)
	{
	  if(n != container_size(B))
	    {
	      return false;
	    }
	}
      else
	{
	  if(n > container_size(B))
	    {
	      n = container_size(B);
	    }
	}
      // Resizing C appropriately
      resize_container(C,n);
      // Getting pointer to C (AFTER resize, as resize may invalidate iterators/pointers)
      auto PC = container_beginning(C);
      op(PC,PA,PB,n);
      return true;
    }

    // Operation On of two vectors with output and success indicator
    template<class CT1, class CT2, class OCT = CT1>
    static inline OCT op(const CT1& containeradded, const CT2& containeradding)
    {
      OCT result = containeradded;
      bool success = op_to(result,containeradding);
      if(!success)
	clear_container(result);
      return result;
    }
  };
}

#endif
