//!Generation of "assignment functors" automatically from elementary operations
//!Makes life easier for elementwise operations, and necessary for some symbolic processing
#ifndef ATMOS_ASSIGN_FUNCT
#define ATMOS_ASSIGN_FUNCT

#include <functional>

// Automatic assignment functor generator
template<class T>
struct binary_assignment
{
  T op;
  template<class OT, class T1, class T2>
  inline OT& operator() (OT& C, const T1& A, const T2& B) {C = op(A,B); return C;}
  template<class OT, class IT>
  inline OT& operator() (OT& A, const IT& B) {A = op(A,B); return A;}
};

// Assignment functors for functions representing operators (e.g +) with assignment operator equivalents (e.g. +=)
template<> struct binary_assignment<std::plus<>>
{
  template<class OT, class T1, class T2>
  inline OT& operator() (OT& C, const T1& A, const T2& B) {C = A + B; return C;}
  template<class OT, class IT>
  inline OT& operator() (OT& A, const IT& B) {A+=B; return A;}
};
template<> struct binary_assignment<std::minus<>>
{
  template<class OT, class T1, class T2>
  inline OT& operator() (OT& C, const T1& A, const T2& B) {C = A - B; return C;}
  template<class OT, class IT>
  inline OT& operator() (OT& A, const IT& B) {A-=B; return A;}
};
template<> struct binary_assignment<std::multiplies<>>
{
  template<class OT, class T1, class T2>
  inline OT& operator() (OT& C, const T1& A, const T2& B) {C = A * B; return C;}
  template<class OT, class IT>
  inline OT& operator() (OT& A, const IT& B) {A*=B; return A;}
};
template<> struct binary_assignment<std::divides<>>
{
  template<class OT, class T1, class T2>
  inline OT& operator() (OT& C, const T1& A, const T2& B) {C = A / B; return C;}
  template<class OT, class IT>
  inline OT& operator() (OT& A, const IT& B) {A/=B; return A;}
};
template<> struct binary_assignment<std::modulus<>>
{
  template<class OT, class T1, class T2>
  inline OT& operator() (OT& C, const T1& A, const T2& B) {C = A % B; return C;}
  template<class OT, class IT>
  inline OT& operator() (OT& A, const IT& B) {A%=B; return A;}
};

#endif
