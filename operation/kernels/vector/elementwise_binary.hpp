// Generalized elementwise binary operation kernel. Uses a binary assignment functor for +=-like operations, which can be specialized to actually use +=

// Header guard. Skips over entire header if already included.
#ifndef ATMOS_KERNEL_ELEM_BIN_OP
#define ATMOS_KERNEL_ELEM_BIN_OP

#include "../../assignment_functor.hpp"

namespace ATMOS
{
  // Elementwise binary operation kernels on contiguous-dense (e.g. incrementing the *iterator* recieved leads to the next element, not necessarily contiguous in *memory*) vector
  template<class functor, class assignment_functor = binary_assignment<functor>, std::size_t defaultStride = 1>
  struct iterator_elementwise_binary_kernel /*public vectorAdditionKernel,*/
  {
    // op_to corresponds to a (op)= b, op corresponds to a (op) b
    // Interface is OUTPUT (omitted if output is returned), DATA, CONTEXT (size, etc.), OPTIONS (stride, transpose, etc.), FUNCTORS (optional, may also be in template parameters)
  
    // PART 1: Simple, basis-free elementwise binary operations:
  
    // Basic op_to:
  
    // STL form (iterator, end iterator, iterator)
    // Unstrided
    template<class iter1, class iter2>
    static inline void op_range_to(iter1 A, const iter1 AEND, iter2 B, assignment_functor assign_op = assignment_functor())
    {
      while(A<AEND)
	{
	  assign_op(*A,*B);
	  std::advance(A,1);
	  std::advance(B,1);
	}
    }
    // Strided
    template<class iter1, class iter2>
    static inline void op_range_to(iter1 A, const iter1 AEND, iter2 B, const std::size_t SA, const std::size_t SB = defaultStride, assignment_functor assign_op = assignment_functor())
    {
      while(A<AEND)
	{
	  assign_op(*A,*B);
	  std::advance(A,SA);
	  std::advance(B,SB);
	}
    }

    // Size form (iterator, iterator, size)
    // Unstrided
    template<class iter1, class iter2>
    static inline void op_to(const iter1 A, const iter2 B, const std::size_t n)
    {
      iter1 E = std::next(A,n);
      op_range_to(A,E,B);
    }
    // Strided
    template<class iter1, class iter2>
    static inline void op_to(const iter1 A, const iter2 B, const std::size_t n, const std::size_t SA, const std::size_t SB = defaultStride)
    {
      iter1 E = std::next(A,n);
      op_range_to(A,E,B,SA,SB);
    }

    // Basic op:

    // STL form (iterator, end iterator, iterator)
    // Unstrided
    template<class outputIter, class iter1, class iter2>
    static inline void op_range(const outputIter C, const iter1 A, const iter1 AE, const iter2 B)
    {
      std::transform(
#ifdef _TMATLIB_CPPZ_EXEC_ENABLED_
		     std::execution::par,
#endif
		     A,AE,B,C,functor());
    }

    // Strided
    template<class outputIter, class iter1, class iter2>
    static inline void op_range(outputIter C, iter1 A, const iter1 AE, iter2 B, const std::size_t SA, const std::size_t SB = defaultStride, const std::size_t SC = defaultStride, functor operation = functor())
    {
      while(A < AE)
	{
	  *C = operation(*A,*B);
	  std::advance(C,SC);
	  std::advance(B,SB);
	  std::advance(A,SA);
	}
    }

    // Size form (iterator, iterator, n)
    // Unstrided
    template<class outputIter, class iter1, class iter2>
    static inline void op(const outputIter C, const iter1 A, const iter2 B, const std::size_t n)
    {
      op_range(C,A,std::next(A,n),B);
    }

    // Strided
    template<class outputIter, class iter1, class iter2>
    static inline void op(const outputIter C, const iter1 A, const iter2 B, const std::size_t n, const std::size_t SA, const std::size_t SB = defaultStride, const std::size_t SC = defaultStride)
    {
      op_range(C,A,std::next(A,n),B,SA,SB,SC);
    }
  };
}

#endif
