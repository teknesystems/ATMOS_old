// Generalized kernel for an elementwise unary operation, e.g. negation

#ifndef ATMOS_KERNEL_ELEM_UNARY_OP
#define ATMOS_KERNEL_ELEM_UNARY_OP

namespace ATMOS
{
  
  template<class functor = std::negate<>, std::size_t defaultStride = 1>
  struct iterator_elementwise_unary_kernel
  {
    template<class OIT, class IT>
    static inline void op(OIT O, IT A, IT AE, const std::size_t stride = defaultStride, const std::size_t resultStride = defaultStride, functor f = functor())
    {
      if(stride==1)
	{
	  while(A!=AE)
	    {
	      *O = f(*A);
	      std::advance(A,1);
	      std::advance(O,resultStride);
	    }
	}
      else
	{
	  while(A<AE)
	    {
	      *O = f(*A);
	      std::advance(A,stride);
	    }
	}
    }
    template<class IT>
    static inline void op(IT A, IT AE, const std::size_t stride = defaultStride, functor f = functor())
    {
      if(stride==1)
	{
	  while(A!=AE)
	    {
	      *A = f(*A);
	      std::advance(A,1);
	    }
	}
      else
	{
	  while(A<AE)
	    {
	      *A = f(*A);
	      std::advance(A,stride);
	    }
	}
  }
    template<class IT>
    static inline void op(IT A, std::size_t n, const std::size_t stride = defaultStride)
    {
      op(A,std::next(A,n),stride);
    }
    template<class OIT, class IT>
    static inline void op(OIT O, IT A, std::size_t n, const std::size_t stride = defaultStride)
    {
      op(O,A,std::next(A,n),stride);
    }
  };
  
}
#endif
