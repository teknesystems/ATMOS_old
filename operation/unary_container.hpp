//!Simple unary operations on containers
#ifndef ATMOS_UNARY_CONTAINER
#define ATMOS_UNARY_CONTAINER

#include "kernels/vector/elementwise_unary.hpp"

namespace ATMOS
{
  
  template<class kernel = iterator_elementwise_unary_kernel<std::negate<>>>
  struct simple_unary_container_operation: public kernel
  {
    using kernel::op;
    template<class CT>
    static inline void op(CT& A)
    {
      op(container_beginning(A),container_end(A));
    }
    template<class CT, class CT2>
    static inline void op(CT& O, const CT2& A)
    {
      if(O.size()<A.size())
	{
	  resize_container(O,A.size());
	}
      op(container_beginning(O),container_beginning(A),container_end(A));
    }
  };
  
}

#endif
